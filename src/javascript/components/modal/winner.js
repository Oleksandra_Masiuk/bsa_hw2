import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import App from '../../app';

export function showWinnerModal(fighter) {
  const bodyElement = createFighterImage(fighter);
  showModal({
    title: `The winner is ${fighter.name}`,
    bodyElement,
    onClose: () => {
      App.rootElement.innerHTML = '';
      App.startApp();
    }
  });
}
